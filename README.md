## Goal

Model sleep schedule repair strategies.

> Early to bed and early to rise makes a man healthy, wealthy, and wise.
>
> -- Benjamin Franklin


## "Coq" Help

> Everything above the bar is what you know to exist or have assumed to exist.
> These are called "hypotheses" and we refer to everything above the bar as
> "the context".
> Below the bar is what we are trying to prove.
> It is called "the current subgoal".
>
> -- nahas_tutorial

* "Tactics" Cheatsheet:
    * https://www.cs.cornell.edu/courses/cs3110/2018sp/a5/coq-tactics-cheatsheet.html
* Naha's Tutorial:
    * http://michaeldnahas.com/doc/nahas_tutorial
* Reference Manual
    * https://coq.inria.fr/distrib/current/refman/proofs/writing-proofs/index.html
