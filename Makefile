compile:
	coqc  ./coq_sleep.v

ide:
	coqide  ./coq_sleep.v

bea: beautify
beautify:
	coqc  -quiet  -beautify ./coq_sleep.v  || true
	mv  coq_sleep.v.beautified  coq_sleep.v
