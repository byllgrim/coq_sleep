Theorem p_implies_p :
  forall p : Prop, p -> p
  .
Proof.
  intros p.        (* Hypothesize p identifier? *)
  intros p_holds.  (* Hypothesize p holds? *)
  exact  p_holds.  (* Apply proof term?  *)
Qed.

(* -------- *)

Record Wakeup := {
  hoursslept : nat;
  clock : nat
}.

Definition isCircadian (wakeup : Wakeup) : bool := true.

Theorem yesCircadian :
  isCircadian {| hoursslept := 8; clock := 0 |} = true.
Proof.
  unfold isCircadian.
  trivial.
Qed.

(* -------- *)

Definition strategyadherence : Prop := false = true.
Theorem tmp : strategyadherence.
Proof.
  split.

Print Prop.

Definition hoursslept : nat := 6.

Definition wellrested : Prop := hoursslept > 8.

Definition wokeearly : bool := true.

Definition circadianachieved : Prop :=
  wellrested /\ (wokeearly = true).

Theorem strategyadherence_implies_circadianachieved :
  (strategyadherence = true) -> circadianachieved
  .
Proof.
  intros strategyadherance.  (* TODO check, don't assume *)
  split.
  unfold wellrested.
  unfold hoursslept.
